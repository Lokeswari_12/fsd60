Auto_Increment
--------------
create table student1 (
sid int primary key auto_increment,
age int);

desc student1;

insert into student1 (age) values (22);
select * from student1;

insert into student1 (age) values (23);
insert into student1 (age) values (24);
select * from student1;

insert into student1 values (101, 12);
select * from student1;

insert into student1 (age) values (23);
insert into student1 (age) values (24);
select * from student1;


drop table student1;



Set the auto_increment value starting from 1001 at the time of table creation
-----------------------------------------------------------------------------
create table student1 (
sid int primary key auto_increment,
age int) auto_increment=1001;

insert into student1 (age) values (22);
insert into student1 (age) values (23);
select * from student1;






Referential Integrity
---------------------
1. On Delete Default
2. On Delete Cascade
3. On Delete Set Null



1. On Delete Default
====================
When you are deleting a record from the parent table and the parent tables corresponding record is associated with the child table record(s), you are not able to delete the parent's table record.

To do it, first you need change the department id of the corresponding records or you need to first delete the corresponding records from the child table, then you can delete the records from the parent table.



Create Department Table
-----------------------
create table department (
deptId int(4) primary key, 
deptName varchar(20),
location varchar(20));

insert into department values 
(10, 'IT', 'Hyd'),
(20, 'Sales', 'Chennai'),
(30, 'Testing', 'Pune');

Select * from department;
