import { Component } from '@angular/core';
import { EmpService } from '../emp.service';


@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrl: './cart.component.css'
})
export class CartComponent {

  cartProducts: any;
  data: any;
  emailId: any;
  total: any;

  
  constructor(private service: EmpService) {
    this.total = 0;
    this.emailId = localStorage.getItem('emailId');
    this.cartProducts = this.service.getCartItems();

    this.cartProducts.forEach((element: any) => {
      this.total = this.total + element.price;
    });
  }

  clearCart() {
    alert('Thank You for Purchasing from our WebSite')
    this.cartProducts = [];
    this.service.setCartItems();
    this.total = 0;
  }

}


