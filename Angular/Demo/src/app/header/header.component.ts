import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrl: './header.component.css'
})
export class HeaderComponent implements OnInit {

  isUserLogged: any;
  cartProducts: any;

  constructor(private service: EmpService) {
    this.cartProducts = this.service.cartItems;
  }

  ngOnInit() {
    this.service.getIsUserLogged().subscribe((data: any) => {
      this.isUserLogged = data;
    });
  }

}


