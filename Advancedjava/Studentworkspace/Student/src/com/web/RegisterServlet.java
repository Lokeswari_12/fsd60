package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.EmployeeDao;
import com.dto.Employee;


@WebServlet("/RegisterServlet")
public class RegisterServlet extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		int empid = Integer.parseInt(request.getParameter("empid"));
		String empname = request.getParameter("empname");
		double salary = Double.parseDouble(request.getParameter("salary"));
		String gender = request.getParameter("gender");
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		Employee emp = new Employee(empid, empname, salary, gender, email, password);
		EmployeeDao empdao = new EmployeeDao();
		int res = empdao.empReg(emp);
		
		if(res > 0) {
			out.print("Registration success");
			RequestDispatcher rd = request.getRequestDispatcher("Login.html");
			rd.include(request, response);
			System.out.println(empid);
		}
		else {
			out.print("Registration Failed");
			System.out.println(empid);
			
			RequestDispatcher rd = request.getRequestDispatcher("Register.html");
			rd.include(request, response);
			
		}
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

}


