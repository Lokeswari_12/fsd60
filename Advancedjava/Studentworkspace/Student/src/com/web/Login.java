
package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.EmployeeDao;
import com.dto.Employee;


@WebServlet("/Login")
public class Login extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		
		if(email.equalsIgnoreCase("harsha@gmail.com") && password.equals("123")) {
			RequestDispatcher rd = request.getRequestDispatcher("HRHomePage");
			rd.forward(request, response);
			
		}
		else {
			EmployeeDao empDao = new EmployeeDao();
			Employee emp = empDao.empLogin(email, password);
			if(emp != null) {
				RequestDispatcher rd = request.getRequestDispatcher("HRHomePage");
				rd.forward(request, response);
				System.out.println(emp);
			}
			else {
			out.println("Login not successful");
			RequestDispatcher rd = request.getRequestDispatcher("Login.html");
			rd.include(request, response);
			}
		}
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

