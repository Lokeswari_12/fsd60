

package com.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import com.db.DbConnection;
import com.dto.Employee;

public class EmployeeDao {
	
	public Employee empLogin(String email, String password) {
		Connection conn = DbConnection.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		String loginQuery = "select * from employee2 where emailId=? and password=?";
		try {
			ps = conn.prepareStatement(loginQuery);
			ps.setString(1, email);
			ps.setString(2, password);
			rs = ps.executeQuery();
			
			if(rs.next()) {
				Employee emp = new Employee();
				emp.setEmpid(rs.getInt(1));
				emp.setEmpname(rs.getString(2));
				emp.setSalary(rs.getDouble(3));
				emp.setGender(rs.getString(4));
				emp.setEmail(rs.getString(5));
				emp.setPassword(rs.getString(6));
				
				return emp;
				
			}
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();
		}
		finally {
			try {
				if (conn != null) {
					rs.close();
					ps.close();
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
		
	}
	public int empReg(Employee emp) {
		// TODO Auto-generated method stub
		Connection con = DbConnection.getConnection();
		PreparedStatement ps = null;
		String regQuery = "insert into employee2 values (?,?,?,?,?,?)";
		try {
			ps = con.prepareStatement(regQuery);
			ps.setInt(1,  emp.getEmpid());
			ps.setString(2, emp.getEmpname());
			ps.setDouble(3, emp.getSalary());
			ps.setString(4, emp.getGender());
			ps.setString(5, emp.getEmail());
			ps.setString(6, emp.getPassword());
			
			return ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			try {
				if (con != null) {
					
					ps.close();
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return 0;
	}
public List<Employee> getAllEmployees() {
		
		Connection connection = DbConnection.getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		
		String loginQuery = "Select * from employee2";
		List<Employee> employeeList = new ArrayList<Employee>();
		
		try {
			preparedStatement = connection.prepareStatement(loginQuery);
			resultSet = preparedStatement.executeQuery();
			
			if (resultSet != null) {
				
				while (resultSet.next()) {
					
					Employee employee = new Employee();
					
					employee.setEmpid(resultSet.getInt(1));
					employee.setEmpname(resultSet.getString(2));
					employee.setSalary(resultSet.getDouble(3));
					employee.setGender(resultSet.getString(4));
					employee.setEmail(resultSet.getString(5));
					employee.setPassword(resultSet.getString(6));
					
					employeeList.add(employee);
				}
				
				return employeeList;
			} 
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {
				if (connection != null) {
					resultSet.close();
					preparedStatement.close();
					connection.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}		
		
		return null;
	}

public Employee getEmployeeById(int empid) {
	// TODO Auto-generated method stub
	Connection connection = DbConnection.getConnection();
	PreparedStatement preparedStatement = null;
	ResultSet resultSet = null;
	
	String loginQuery = "Select * from employee2 where empId=?";
	
	
	
	try {
		preparedStatement = connection.prepareStatement(loginQuery);
		preparedStatement.setInt(1, empid);
		resultSet = preparedStatement.executeQuery();
		
		if (resultSet.next()) {
			
			
				
				Employee employee = new Employee();
				
				employee.setEmpid(resultSet.getInt(1));
				employee.setEmpname(resultSet.getString(2));
				employee.setSalary(resultSet.getDouble(3));
				employee.setGender(resultSet.getString(4));
				employee.setEmail(resultSet.getString(5));
				employee.setPassword(resultSet.getString(6));
				
				
			
			
			return employee;
		} 
		
	} catch (SQLException e) {
		e.printStackTrace();
	}
	
	finally {
		try {
			if (connection != null) {
				resultSet.close();
				preparedStatement.close();
				connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}		
	
	return null;
}	


	
	

}