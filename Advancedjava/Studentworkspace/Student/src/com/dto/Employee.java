package com.dto;
public class Employee {
	private int empid;
	private String empname;
	private double salary;
	private String gender;
	
	private String email;
	private String password;
	
	public Employee() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Employee(int empid, String empname, double salary, String gender, String email, String password) {
		super();
		this.empid = empid;
		this.empname = empname;
		this.salary = salary;
		this.gender = gender;
		this.email = email;
		this.password = password;
	}
	public int getEmpid() {
		return empid;
	}
	public void setEmpid(int empid) {
		this.empid = empid;
	}
	public String getEmpname() {
		return empname;
	}
	public void setEmpname(String empname) {
		this.empname = empname;
	}
	public double getSalary() {
		return salary;
	}
	public void setSalary(double salary) {
		this.salary = salary;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	@Override
	public String toString() {
		return "Employee [empId=" + empid + ", empName=" + empname + ", salary=" + salary + ", gender=" + gender
				+ ", emailId=" + email + ", password=" + password + "]";
	}
	
	
}

